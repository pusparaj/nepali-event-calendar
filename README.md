# It is an nepali event calendar and also convert nepali date into english and vice versa
## Installation

First add following code in your composer.json:
```bash
   "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/pusparaj/nepali-event-calendar"
        }
    ],
```

Then install the package via composer:
```bash
composer require pusparaj/nepalicalendar
```
Use providers:
```bash
Pusparaj\NepaliCalendar\NepaliEventCalendarServiceProvider::class
```
Use aliases:
```bash
'NepaliEventCalendar' => Pusparaj\NepaliCalendar\Facades\NepaliEventCalendar::class
```
# Basic Usage

Convert AD to BS

```bash
use Pusparaj\NepaliCalendar\NepaliDateConverter;

$bsConvetedDate = NepaliDateConverter::englishToNepaliDateConverter(2023,10,20);
```
Convert BS to AD
```bash
$asConvertedDate = NepaliDateConverter::nepaliToEnglishDateConverter(2076,01,23);
```
# To show nepali event calendar
In Controller
```bash
use Pusparaj\NepaliCalendar\NepaliEventCalendar;

 $events = array(
 '2080-1-23'=>['Test Event1','Test Event2'],
 '2080-7-10'=>['Test Event 3'],
 );
 $nepaliCalendar = NepaliEventCalendar::generateNepaliCalendar($events);
```
<strong>Note :</strong> <i>You can set empty array() for event. Also add multiple events for same day like above</i>

In View file

You can simply use following code.
```bash
{!! $nepaliCalendar !!}
```