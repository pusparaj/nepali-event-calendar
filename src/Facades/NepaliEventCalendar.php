<?php

namespace Pusparaj\NepaliCalendar\Facades;

use Illuminate\Support\Facades\Facade;

class NepaliEventCalendar extends Facade
 {

    protected static function getFacadeAccessor()
    {
        return 'nepalieventcalendar';
    }

 }
