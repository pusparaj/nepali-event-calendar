<?php

namespace Pusparaj\NepaliCalendar;

class NepaliEventCalendar
{
    protected static $_bs = array(
        0 => array(2000,30,32,31,32,31,30,30,30,29,30,29,31),
        1 => array(2001,31,31,32,31,31,31,30,29,30,29,30,30),
        2 => array(2002,31,31,32,32,31,30,30,29,30,29,30,30),
        3 => array(2003,31,32,31,32,31,30,30,30,29,29,30,31),
        4 => array(2004,30,32,31,32,31,30,30,30,29,30,29,31),
        5 => array(2005,31,31,32,31,31,31,30,29,30,29,30,30),
        6 => array(2006,31,31,32,32,31,30,30,29,30,29,30,30),
        7 => array(2007,31,32,31,32,31,30,30,30,29,29,30,31),
        8 => array(2008,31,31,31,32,31,31,29,30,30,29,29,31),
        9 => array(2009,31,31,32,31,31,31,30,29,30,29,30,30),
        10 => array(2010,31,31,32,32,31,30,30,29,30,29,30,30),
        11 => array(2011,31,32,31,32,31,30,30,30,29,29,30,31),
        12 => array(2012,31,31,31,32,31,31,29,30,30,29,30,30),
        13 => array(2013,31,31,32,31,31,31,30,29,30,29,30,30),
        14 => array(2014,31,31,32,32,31,30,30,29,30,29,30,30),
        15 => array(2015,31,32,31,32,31,30,30,30,29,29,30,31),
        16 => array(2016,31,31,31,32,31,31,29,30,30,29,30,30),
        17 => array(2017,31,31,32,31,31,31,30,29,30,29,30,30),
        18 => array(2018,31,32,31,32,31,30,30,29,30,29,30,30),
        19 => array(2019,31,32,31,32,31,30,30,30,29,30,29,31),
        20 => array(2020,31,31,31,32,31,31,30,29,30,29,30,30),
        21 => array(2021,31,31,32,31,31,31,30,29,30,29,30,30),
        22 => array(2022,31,32,31,32,31,30,30,30,29,29,30,30),
        23 => array(2023,31,32,31,32,31,30,30,30,29,30,29,31),
        24 => array(2024,31,31,31,32,31,31,30,29,30,29,30,30),
        25 => array(2025,31,31,32,31,31,31,30,29,30,29,30,30),
        26 => array(2026,31,32,31,32,31,30,30,30,29,29,30,31),
        27 => array(2027,30,32,31,32,31,30,30,30,29,30,29,31),
        28 => array(2028,31,31,32,31,31,31,30,29,30,29,30,30),
        29 => array(2029,31,31,32,31,32,30,30,29,30,29,30,30),
        30 => array(2030,31,32,31,32,31,30,30,30,29,29,30,31),
        31 => array(2031,30,32,31,32,31,30,30,30,29,30,29,31),
        32 => array(2032,31,31,32,31,31,31,30,29,30,29,30,30),
        33 => array(2033,31,31,32,32,31,30,30,29,30,29,30,30),
        34 => array(2034,31,32,31,32,31,30,30,30,29,29,30,31),
        35 => array(2035,30,32,31,32,31,31,29,30,30,29,29,31),
        36 => array(2036,31,31,32,31,31,31,30,29,30,29,30,30),
        37 => array(2037,31,31,32,32,31,30,30,29,30,29,30,30),
        38 => array(2038,31,32,31,32,31,30,30,30,29,29,30,31),
        39 => array(2039,31,31,31,32,31,31,29,30,30,29,30,30),
        40 => array(2040,31,31,32,31,31,31,30,29,30,29,30,30),
        41 => array(2041,31,31,32,32,31,30,30,29,30,29,30,30),
        42 => array(2042,31,32,31,32,31,30,30,30,29,29,30,31),
        43 => array(2043,31,31,31,32,31,31,29,30,30,29,30,30),
        44 => array(2044,31,31,32,31,31,31,30,29,30,29,30,30),
        45 => array(2045,31,32,31,32,31,30,30,29,30,29,30,30),
        46 => array(2046,31,32,31,32,31,30,30,30,29,29,30,31),
        47 => array(2047,31,31,31,32,31,31,30,29,30,29,30,30),
        48 => array(2048,31,31,32,31,31,31,30,29,30,29,30,30),
        49 => array(2049,31,32,31,32,31,30,30,30,29,29,30,30),
        50 => array(2050,31,32,31,32,31,30,30,30,29,30,29,31),
        51 => array(2051,31,31,31,32,31,31,30,29,30,29,30,30),
        52 => array(2052,31,31,32,31,31,31,30,29,30,29,30,30),
        53 => array(2053,31,32,31,32,31,30,30,30,29,29,30,30),
        54 => array(2054,31,32,31,32,31,30,30,30,29,30,29,31),
        55 => array(2055,31,31,32,31,31,31,30,29,30,29,30,30),
        56 => array(2056,31,31,32,31,32,30,30,29,30,29,30,30),
        57 => array(2057,31,32,31,32,31,30,30,30,29,29,30,31),
        58 => array(2058,30,32,31,32,31,30,30,30,29,30,29,31),
        59 => array(2059,31,31,32,31,31,31,30,29,30,29,30,30),
        60 => array(2060,31,31,32,32,31,30,30,29,30,29,30,30),
        61 => array(2061,31,32,31,32,31,30,30,30,29,29,30,31),
        62 => array(2062,30,32,31,32,31,31,29,30,29,30,29,31),
        63 => array(2063,31,31,32,31,31,31,30,29,30,29,30,30),
        64 => array(2064,31,31,32,32,31,30,30,29,30,29,30,30),
        65 => array(2065,31,32,31,32,31,30,30,30,29,29,30,31),
        66 => array(2066,31,31,31,32,31,31,29,30,30,29,29,31),
        67 => array(2067,31,31,32,31,31,31,30,29,30,29,30,30),
        68 => array(2068,31,31,32,32,31,30,30,29,30,29,30,30),
        69 => array(2069,31,32,31,32,31,30,30,30,29,29,30,31),
        70 => array(2070,31,31,31,32,31,31,29,30,30,29,30,30),
        71 => array(2071,31,31,32,31,31,31,30,29,30,29,30,30),
        72 => array(2072,31,32,31,32,31,30,30,29,30,29,30,30),
        73 => array(2073,31,32,31,32,31,30,30,30,29,29,30,31),
        74 => array(2074,31,31,31,32,31,31,30,29,30,29,30,30),
        75 => array(2075,31,31,32,31,31,31,30,29,30,29,30,30),
        76 => array(2076,31,32,31,32,31,30,30,30,29,29,30,30),
        77 => array(2077,31,32,31,32,31,30,30,30,29,30,29,31),
        78 => array(2078,31,31,31,32,31,31,30,29,30,29,30,30),
        79 => array(2079,31,31,32,31,31,31,30,29,30,29,30,30),
        80 => array(2080,31,32,31,32,31,30,30,30,29,29,30,30),
        81 => array(2081,31,31,32,32,31,30,30,30,29,30,30,30),
        82 => array(2082,30,32,31,32,31,30,30,30,29,30,30,30),
        83 => array(2083,31,31,32,31,31,30,30,30,29,30,30,30),
        84 => array(2084,31,31,32,31,31,30,30,30,29,30,30,30),
        85 => array(2085,31,32,31,32,30,31,30,30,29,30,30,30),
        86 => array(2086,30,32,31,32,31,30,30,30,29,30,30,30),
        87 => array(2087,31,31,32,31,31,31,30,30,29,30,30,30),
        88 => array(2088,30,31,32,32,30,31,30,30,29,30,30,30),
        89 => array(2089,30,32,31,32,31,30,30,30,29,30,30,30),
        90 => array(2090,30,32,31,32,31,30,30,30,29,30,30,30),
        91 => array(2091,31,31,32,32,31,30,30,29,30,29,30,30),
        92 => array(2092,30,31,32,32,31,30,30,30,29,30,30,30),
        93 => array(2093,30,32,31,32,31,30,30,30,29,29,30,30),
        94 => array(2094,31,31,32,31,31,30,30,30,29,30,30,30),
        95 => array(2095,31,31,32,31,31,31,30,29,30,30,30,30)
    );

    protected static $_nepali_years = array(
        '2000'=> '२०००',
        '2001'=> '२००१',
        '2002'=> '२००२',
        '2003'=> '२००३',
        '2004'=> '२००४',
        '2005'=> '२००५',
        '2006'=> '२००६',
        '2007'=> '२००७',
        '2008'=> '२००८',
        '2009'=> '२००९',
        '2010'=> '२०१०',
        '2011'=> '२०११',
        '2012'=> '२०१२',
        '2013'=> '२०१३',
        '2014'=> '२०१४',
        '2015'=> '२०१५',
        '2016'=> '२०१६',
        '2017'=> '२०१७',
        '2018'=> '२०१८',
        '2019'=> '२०१९',
        '2020'=> '२०२०',
        '2021'=> '२०२१',
        '2022'=> '२०२२',
        '2023'=> '२०२३',
        '2024'=> '२०२४',
        '2025'=> '२०२५',
        '2026'=> '२०२६',
        '2027'=> '२०२७',
        '2028'=> '२०२८',
        '2029'=> '२०२९',
        '2030'=> '२०३०',
        '2031'=> '२०३१',
        '2032'=> '२०३२',
        '2033'=> '२०३३',
        '2034'=> '२०३४',
        '2035'=> '२०३५',
        '2036'=> '२०३६',
        '2037'=> '२०३७',
        '2038'=> '२०३८',
        '2039'=> '२०३९',
        '2040'=> '२०४०',
        '2041'=> '२०४१',
        '2042'=> '२०४२',
        '2043'=> '२०४३',
        '2044'=> '२०४४',
        '2045'=> '२०४५',
        '2046'=> '२०४६',
        '2047'=> '२०४७',
        '2048'=> '२०४८',
        '2049'=> '२०४९',
        '2050'=> '२०५०',
        '2051'=> '२०५१',
        '2052'=> '२०५२',
        '2053'=> '२०५३',
        '2054'=> '२०५४',
        '2055'=> '२०५५',
        '2056'=> '२०५६',
        '2057'=> '२०५७',
        '2058'=> '२०५८',
        '2059'=> '२०५९',
        '2060'=> '२०६०',
        '2061'=> '२०६१',
        '2062'=> '२०६२',
        '2063'=> '२०६३',
        '2064'=> '२०६४',
        '2065'=> '२०६५',
        '2066'=> '२०६६',
        '2067'=> '२०६७',
        '2068'=> '२०६८',
        '2069'=> '२०६९',
        '2070'=> '२०७०',
        '2071'=> '२०७१',
        '2072'=> '२०७२',
        '2073'=> '२०७३',
        '2074'=> '२०७४',
        '2075'=> '२०७५',
        '2076'=> '२०७६',
        '2077'=> '२०७७',
        '2078'=> '२०७८',
        '2079'=> '२०७९',
        '2080'=> '२०८०',
        '2081'=> '२०८१',
        '2082'=> '२०८२',
        '2083'=> '२०८३',
        '2084'=> '२०८४',
        '2085'=> '२०८५',
        '2086'=> '२०८६',
        '2087'=> '२०८७',
        '2088'=> '२०८८',
        '2089'=> '२०८९',
        '2090'=> '२०९०',
        '2091'=> '२०९१',
        '2092'=> '२०९२',
        '2093'=> '२०९३',
        '2094'=> '२०९४',
        '2095'=> '२०९५'
    );

    protected static $_nepali_months = array(
        1 => 'बैशाख',
        2 => 'जेष्ठ',
        3 => 'आषाढ',
        4 => 'श्रावण',
        5 => 'भाद्र',
        6 => 'आश्विन',
        7 => 'कार्तिक',
        8 => 'मंसिर',
        9 => 'पौष',
        10 => 'माघ',
        11 => 'फाल्गुन',
        12 => 'चैत्र'
    );

    protected static $_nepali_weeks = array(
        1 => 'आइतबार',
        2 => 'सोमबार',
        3 => 'मङ्गलबार',
        4 => 'बुधबार',
        5 => 'बिहिबार',
        6 => 'शुक्रबार',
        7 => 'शनिबार'
    );


    protected static $_nepali_days = array(
        1 => '१',
        2 => '२',
        3 => '३',
        4 => '४',
        5 => '५',
        6 => '६',
        7 => '७',
        8 => '८',
        9 => '९',
        10 => '१०',
        11 => '११',
        12 => '१२',
        13 => '१३',
        14 => '१४',
        15 => '१५',
        16 => '१६',
        17 => '१७',
        18 => '१८',
        19 => '१९',
        20 => '२०',
        21 => '२१',
        22 => '२२',
        23 => '२३',
        24 => '२४',
        25 => '२५',
        26 => '२६',
        27 => '२७',
        28 => '२८',
        29 => '२९',
        30 => '३०',
        31 => '३१',
        32 => '३२'
    );
    public static function generateNepaliCalendar($nep_events = null)
    {
        $todayDate = NepaliDateConverter::englishToNepaliDateConverter(date('Y'), date('n'), date('j'));
        $todayYear = $todayDate['year'];
        $todayMonth = $todayDate['month'];
        $todayDay = $todayDate['day'];
        $todaySelectedDay ="$todayYear-$todayMonth-$todayDay";

        $selectedMonth = $_GET['nepali_month'] ?? $todayMonth;
        $selectedYear = $_GET['nepali_year'] ?? $todayYear;

        $currentMonthName = self::$_nepali_months[$selectedMonth];
        $currentYear = self::$_nepali_years[$selectedYear];

        $form = '<div class="centered-form" style="display: flex; justify-content: space-between; align-items: center; margin-top: 10px; margin-bottom: 20px;">';
        $form .= '<div style="display: flex; align-items: center;">';
        $form .= '<div style="font-weight: bold; margin-right: 10px;">' . $currentMonthName . ' ' . $currentYear . '</div>';
        $form .= '</div>';

        $form .= '<form method="get" style="display: flex; align-items: center;">';
        $form .= '<label for="nepali_year" style="margin-right: 10px;">वर्ष: </label>';
        $form .= '<select name="nepali_year" id="nepali_year" onchange="this.form.submit()" style="padding: 3px; margin-right: 10px; border: 1px solid #ccc; border-radius: 5px;">';
        foreach (self::$_bs as $months) {
            $englishFormYear = $months[0];
            $formYear = self::$_nepali_years[$englishFormYear];
            $form .= "<option value=\"$englishFormYear\"";
            if ($englishFormYear == $selectedYear) {
                $form .= " selected";
            }
            $form .= ">$formYear</option>";
        }
        $form .= '</select>';

        $form .= '<label for="nepali_month" style="margin-right: 10px;">महिना: </label>';
        $form .= '<select name="nepali_month" id="nepali_month" onchange="this.form.submit()" style="padding: 3px; margin-right: 10px; border: 1px solid #ccc; border-radius: 5px;">';
        for ($i = 1; $i <= 12; $i++) {
            $monthName = self::$_nepali_months[$i];
            $form .= "<option value=\"$i\"";
            if ($i == $selectedMonth) {
                $form .= " selected";
            }
            $form .= ">$monthName</option>";
        }
        $form .= '</select>';
        $form .= '</form>';
        $form .= '</div>';

        $calendar = $form . self::generateCalendarTable($selectedYear, $selectedMonth, $todaySelectedDay, $nep_events);
        return '<div class="nepali-calendar">' . $calendar . '</div>';
    }


    public static function generateCalendarTable($year, $month, $todaySelectedDay, $nep_events)
    {
        // Check if the year exists in the _bs array
        foreach (self::$_bs as $index => $months) {
            $nepaliYear = $months[0];
            if ($nepaliYear == $year) {
                // Check if the month exists in the current year's data
                if (array_key_exists($month, $months)) {
                    $daysInMonth = $months[$month];

                    // Calculate the first day of the month
                    $convertedEnglishDate = NepaliDateConverter::nepaliToEnglishDateConverter($nepaliYear,$month,01);
                    $firstDayOfMonth = date("N", strtotime("{$convertedEnglishDate['year']}-{$convertedEnglishDate['month']}-{$convertedEnglishDate['day']}")) +1;

                    // Generate the calendar HTML
                    $calendar = '<table border="1" style="width: 100%;">';
                    $calendar .= '<tr>';
                    foreach ( self::$_nepali_weeks as $w => $week){
                        //for saturday
                        if($w == 7){
                            $calendar .= '<th style="text-align: center;color: red;">' . $week . '</th>';
                        }else{
                            $calendar .= '<th style="text-align: center;">' . $week . '</th>';
                        }
                    }
                    $calendar .= '</tr><tr>';
                    $currentDay = 1;
                    for ($day = 1; $day <= 7; $day++) {
                        if ($day < $firstDayOfMonth) {
                            $calendar .= '<td></td>';
                        } else {
                            $date = "$year-$month-$currentDay";

                            // Check if the day is Saturday (7 corresponds to Saturday, assuming 1 is Sunday)
                            $isWeekend = $day == 7;
                            $hasEvents = array_key_exists($date, $nep_events);

                            // Apply red color style for today/weekend and dates with events
                            $cellStyle = 'text-align: center;';
                            if ($isWeekend || $hasEvents) {
                                $cellStyle .= 'color: red;';
                            }
                            if ($todaySelectedDay == $date) {
                                $cellStyle .= 'font-weight: bold;background-color: blue;border-radius: 50%;color: #ffffff;display: inline-block;line-height: 30px;width: 30px; ';
                            }
                            $calendar .= '<td style="width: 10px;height: 80px;text-align: center;vertical-align: middle;border: 1px solid black;color:red;"><span style="' . $cellStyle . '">' . self::$_nepali_days[$currentDay].'</span>';

                            // Check if there is an event for the current date
                            // Check if there is an event for the current date
                            if ($hasEvents) {
                                $events = $nep_events[$date];
                                $calendar .= '<ul style="list-style-type: disc; text-align: left; margin-left: 10px;">';
                                foreach ($events as $event) {
                                    $calendar .= '<li>' . $event . '</li>';
                                }
                                $calendar .= '</ul>';
                            }


                            $calendar .= '</td>';
                            $currentDay++;
                        }
                    }
                    $calendar .= '</tr>';

                    for ($row = 2; $row <= 6; $row++) {
                        $calendar .= '<tr>';
                        for ($day = 1; $day <= 7; $day++) {
                            if ($currentDay <= $daysInMonth) {
                                $date = "$year-$month-$currentDay";

                                // Check if the day is Saturday (7 corresponds to Saturday, assuming 1 is Sunday)
                                $isWeekend = $day == 7;
                                $hasEvents = array_key_exists($date, $nep_events);

                                // Apply red color style for today/weekend and dates with events
                                $cellStyle = 'text-align: center;';
                                if ($isWeekend || $hasEvents) {
                                    $cellStyle .= 'color: red;';
                                }
                                if ($todaySelectedDay == $date) {
                                    $cellStyle .= 'font-weight: bold;background-color: blue;border-radius: 50%;color: #ffffff;display: inline-block;line-height: 30px;width: 30px; ';
                                }
                                $calendar .= '<td style="width: 10px;height: 80px;text-align: center;vertical-align: middle;border: 1px solid black;"><span style="' . $cellStyle . '">' . self::$_nepali_days[$currentDay].'</span>';

                                // Check if there is an event for the current date
                                if ($hasEvents) {
                                    $events = $nep_events[$date];
                                    $calendar .= '<ul style="list-style-type: disc; text-align: left; font-size: 12px; line-height: 1; padding-left: 18px;color:red;">';
                                    foreach ($events as $event) {
                                        $calendar .= '<li style="margin-bottom:-10px;">' . $event . '</li>';
                                    }
                                    $calendar .= '</ul>';
                                }


                                $calendar .= '</td>';
                                $currentDay++;
                            }
                        }
                        $calendar .= '</tr>';
                    }

                    $calendar .= '</table>';
                    return $calendar;
                } else {
                    return "Invalid month for the specified year.";
                }
            }
        }

        return "Invalid year.";
    }

}
